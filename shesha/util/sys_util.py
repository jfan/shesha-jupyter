import GPUtil
import psutil
from time import sleep

def sys_load(gpu_ids=[0]):
    gpus = [gpu for gpu in GPUtil.getGPUs() if gpu.id in gpu_ids]

    load_cpu = psutil.cpu_percent(interval=1)
    load_gpu = sum([gpu.load for gpu in gpus]) / len(gpus) * 100
    return load_cpu, load_gpu

def wait_for_idle(threshold_cpu, threshold_gpu, devices):
    is_idle = False
    while not is_idle:
        print("Waiting until CPU and GPU idle.... (< {}% CPU < {}% GPU)".format(threshold_cpu, threshold_gpu))

        load_cpu, load_gpu = sys_load(gpu_ids=devices)
        print("CPU at {:.1f}% and GPU at {:.1f}%".format(load_cpu, load_gpu))

        if load_cpu < threshold_cpu and load_gpu < threshold_gpu:
            print("Wait is over, let's go")
            is_idle = True
        else:
            sleep(2)
